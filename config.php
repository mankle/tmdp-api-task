<?php

class config {
	const DB_SERVER      = 'localhost';
	const DB_NAME        = 'movies';
	const DB_USERNAME    = 'root';
	const DB_PASSWORD    = '';
	const DB_PREFIX	     = '';
	const TMDB_KEY	     = '2a05e16e770c035f04b7c9aec43906a0';
	const TMDB_ADDRESS   = 'https://api.themoviedb.org/3';
	const TABLE_REQUESTS = 'requests';
	const TABLE_RESULTS  = 'results';
}

?>
