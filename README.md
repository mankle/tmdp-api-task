Užduotis:
1. Sukurti paprasta filmų paieškos aplikaciją naudojant themoviedb.org API. 
Kiekviena paieškos užklausa ir jos rezultatas (jei jis sėkmingas ir yra bent vienas įrašas) turėtų būti įrašomas į duomenų bazę (MySQL). 
Programai įgyvendinti galima naudoti pasirinkta PHP frameworką (arba nenaudoti visai).
2. Programa turėtų turėti šiuos routus: 
GET /                    - paieškos laukelis bei paieškos rezultatai
GET /history             - atvaizduoti visas paieškos užklausas
------- Bonus (PAPILDOMA UŽDUOTIS) ------
GET /history/{id:[0-9]+} - atvaizduoti pasirinkta paieškos rezultatą iš DB
3. Front-end stilius nebūtinas. Svarbiausia back-end dalis.