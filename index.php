<?php

	session_start();
	//database and other misc. configurations 
	include "config.php";
	include "mysql.php";
	include "model/model.php";
	include "controller/controller.php";
	include "view/view.php";

?>
<p><a href="index.php">SEARCH</a> <a href="index.php?page=history">HISTORY</a></p>



<?php

	//class names of model, view and controller
	$files = array(
		'search' => array('model' => 'SearchModel', 'view' => 'SearchView', 'controller' => 'SearchController'),
		'history' => array('model' => 'HistoryModel', 'view' => 'HistoryView', 'controller' => 'HistoryController'),
		'info' => array('model' => 'InfoModel', 'view' => 'InfoView', 'controller' => 'InfoController'));

	//if page is set choose appropriate class
	if (isset($_GET['page'])) {
		foreach($files as $key => $components){
        	if ($_GET['page'] == $key) {
            	$model = $components['model'];
            	$view = $components['view'];
            	$controller = $components['controller'];
            	break;
        	}
    	}

	} else { //if page is not set open default search page
		$model = $files['search']['model'];
		$view = $files['search']['view'];
		$controller = $files['search']['controller'];
	}

	//set model, view and controller classes and output html code from view
	if (isset($model)) {
        $m = new $model();
        $c = new $controller($m);
        $v = new $view($c, $m);
    	echo $v->output();
    }

    //if search was submited get the data and refresh page
    if(!empty($_POST['submit_search'])) {
    	$c->search_movie();
    	header("Refresh:0");
    }
?>
