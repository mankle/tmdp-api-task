<?php
class Model
{
    public $string;

    public function __construct(){
        $this->string = "";
    }

}

class SearchModel extends Model
{
	public $last_request_id;

	public function __construct(){
		//parent::__construct();
        $this->string = "";
        $this->last_request_id = "";
    }

    //inserts request query into table of requests
	public function insertRequest($request){
		$query = "  INSERT INTO " . config::TABLE_REQUESTS . "
			(`request`) VALUES ('{$request}')";
		mysql::query($query);

		//get inserted request id for result table
		$this->last_request_id = mysql::getLastInsertedId();

		$_SESSION['last_request_id'] = $this->last_request_id;
	}

	public function getRequest($id){
		$query = "SELECT `request` FROM " . config::TABLE_REQUESTS . " WHERE `id`={$id}";
		return mysql::select($query);
	}

	//inserts result id into table of results
	public function insertResult($movie_id, $request_id, $title){
		$query = "  INSERT INTO " . config::TABLE_RESULTS . "
			(`request_id`, `movie_id`, `title`) VALUES ({$request_id}, {$movie_id}, '{$title}')";
		mysql::query($query);
	}

	//returns array of request results
	public function getResults($request_id){
		$query = "SELECT `movie_id`, `title` FROM " . config::TABLE_RESULTS . " WHERE `request_id`={$request_id}";
		return mysql::select($query);
	}

	//creates string of html code that contains request word and all results with links
	public function convertResultArrayToString($request_id){
		$result_data = $this->getResults($request_id);
		$request = $this->getRequest($request_id);
		$this->string = "<p>You searched for: {$request[0]['request']}</p><p>Results:</p>";
		foreach ($result_data as $value) {
			$this->string = $this->string . '<p><a href="index.php?page=info&id=' . $value['movie_id'] . '" >' . $value['title'] . '</a></p>'; 
		}
	}
}

class HistoryModel extends Model
{
	public function __construct(){
        $this->string = "";
        $this->last_request_id = "";
    }

    //returns array of all requests
    public function getRequests(){
		$query = "SELECT `id`, `request` FROM " . config::TABLE_REQUESTS;
		return mysql::select($query);
	}

	//creates string of html code that contains request word and all results with links
	public function getRequestArrayString(){
		$request_data = $this->getRequests();
		$this->string = "";
		foreach ($request_data as $value) {
			$this->string = $this->string . '<p><a href="index.php?page=info&id=' . $value['id'] . '">' . $value['request'] . '</a></p>'; 
		}
	}

}

class InfoModel extends Model
{
	public function __construct(){
        $this->string = "";
    }

    //returns array of given request results
    public function getResults($request_id){
		$query = "SELECT `movie_id`, `title` FROM " . config::TABLE_RESULTS . " WHERE `request_id`='{$request_id}'";
		return mysql::select($query);
	}

	//forms html code with all the titles of results
	public function getResultsArrayString($request_id){
		$result_data = $this->getResults($request_id);
		$this->string = "";
		foreach ($result_data as $value) {
			$this->string = $this->string . '<p>' . $value['title'] . '</p>'; 
		}
	}

}