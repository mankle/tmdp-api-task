<?php
class View
{
    private $model;
    private $controller;

    public function __construct($controller,$model) {
        $this->controller = $controller;
        $this->model = $model;
    }

    public function output() {
        return '';
    }
}

class SearchView extends View
{
    public function __construct($controller,$model) {
        $this->controller = $controller;
        $this->model = $model;
    }

    public function output() {
        $output = '<form action="" method="post">
            <label class="field" for="query">Search Movie</label>
            <input type="text" id="query" name="query" class="textbox textbox-150">
            <input type="submit" name="submit_search" value="Search">
            </form>';
    
        if(isset($_SESSION['last_request_id'])){
            $output .= $this->outputLinks();
        }
        return $output;
    }

    public function outputLinks(){
        $this->model->convertResultArrayToString($_SESSION['last_request_id']);
        return $this->model->string;
    }
}

class HistoryView extends View
{
    public function __construct($controller,$model) {
        $this->controller = $controller;
        $this->model = $model;
    }

    public function output() {
        $this->model->getRequestArrayString();
        return $this->model->string;
    }

}

class InfoView extends View
{
    public function __construct($controller,$model) {
        $this->controller = $controller;
        $this->model = $model;
    }

    public function output() {
        $this->model->getResultsArrayString($_GET['id']);
        return $this->model->string;
    }

}