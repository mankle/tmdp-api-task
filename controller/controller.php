<?php
class Controller
{
    private $model;

    public function __construct($model){
        $this->model = $model;
    }
}

class SearchController extends Controller
{
	private $api_query;

    public function __construct($model){
        $this->model = $model;
    }

	public function search_movie(){
		if (!empty($_POST['submit_search'])) {
			$this->getResults($_POST);
		}
	}

	public function getResults($data){
		//get results of tmdb query 
		$result_data = file_get_contents($this->getRequest($data['query'], 1));

		//if fails to get results function is newt_finished
		if ($result_data == false) {
			return;
		}
		//decode result json to php array
		$result_data = json_decode($result_data);

		//read data from result array 
		$total_pages = $result_data->total_pages;
		$total_results = $result_data->total_results;

		//run code only if there are results 
		if ($total_results > 0) {

			//insert request query into request table
			$this->model->insertRequest($data['query']);

			//insert all restults into result table
			foreach ($result_data->results as $result) {
				$this->model->insertResult($result->id, $this->model->last_request_id, $result->title);
			}

			//if there is more than 1 page of results get data from other pages and insert results into result table
			if ($total_pages > 1) {
				for ($i=2; $i <= $total_pages; $i++) { 
					//get results of tmdb query 
					$result_data = file_get_contents($this->getRequest($data['query'], $i));

					//decode result json to php array
					$result_data = json_decode($result_data);

					//insert all restults into result table
					foreach ($result_data->results as $result) {
						$this->model->insertResult($result->id, $this->model->last_request_id, $result->title);
					}
				}
			}
		}		
	}

	//generate request uri
	private function getRequest($query, $page){
		if ($page != 1) {
			return config::TMDB_ADDRESS . "/search/movie?api_key=" . config::TMDB_KEY . "&query={$query}&page={$page}";
		} else {
			return config::TMDB_ADDRESS . "/search/movie?api_key=" . config::TMDB_KEY . "&query={$query}";
		}
	}
}

class HistoryController extends Controller
{
	public function __construct($model){
        $this->model = $model;
    }
}


class InfoController extends Controller
{
	public function __construct($model){
        $this->model = $model;
    }
}